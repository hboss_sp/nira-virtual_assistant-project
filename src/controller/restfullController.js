

	var express = require("express");
	var bodyParser = require('body-parser');
	var models = require("../model/daoFactory");
	
	/*
	 * Método que sobe os endpoitns de CRUD
	 */
	function publishRestfull(app){
		
		app.use(bodyParser.json()); // support json encoded bodies
		app.use(bodyParser.urlencoded({ extended: true }));

		app.get("/services/:entityname", function(request, response){
			
			response.setHeader('Access-Control-Allow-Origin', '*');
			
			var entityname = request.params.entityname;
			var to = require("../businessdelegate/entityDataBusinessDelegate")[entityname];
			
			if( to["anotherFind"] ){
				to["loadFindAll"](response);
			}	
			else{
				models[ entityname ].findAll().then(list => {
					response.send( list );
			}	);
			}
			
			
		});

		app.get("/services/:entityname/:entityid", function(request, response){
			
			response.setHeader('Access-Control-Allow-Origin', '*');
			
			var entityname = request.params.entityname;
			var entityid = request.params.entityid;
			
			models[ entityname ].findById(entityid).then(obj => {
				response.send( obj );
			});
		});
		
		app.get("/services/:entityname/collumns/meta", function(request, response){
			
			response.setHeader('Access-Control-Allow-Origin', '*');
			
			var entityname = request.params.entityname;
			var columns = require("../businessdelegate/entityDataBusinessDelegate")[entityname].getCollumns()
			response.send( columns );
			
		});

		app.post("/services/:entityname", function(request, response){
			
			response.setHeader('Access-Control-Allow-Origin', '*');
			
			var entityname = request.params.entityname;
			models[ entityname ].create( request.body ).then(obj => {
				response.send( obj );
			});
		});

		app.post("/services/:entityname/:entityid", function(request, response){
			
			response.setHeader('Access-Control-Allow-Origin', '*');
			
			var entityname = request.params.entityname;
			var entityid = request.params.entityid
			
			models[ entityname ].update( request.body, { where : { "id" : entityid} } ).then(obj => {
				response.send( obj );
			});
		});

		app.delete("/services/:entityname/:entityid", function(request, response){
			
			response.setHeader('Access-Control-Allow-Origin', '*');
			
			var entityname = request.params.entityname;
			var entityid = request.params.entityid
			
			models[ entityname ].destroy( { where : { "id" : entityid} } );
			response.send();
		});
		
	}
	
	module.exports = publishRestfull;