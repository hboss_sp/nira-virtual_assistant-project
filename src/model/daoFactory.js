

	var Sequelize = require("sequelize");
	var sequelize = new Sequelize("app_salon", "root", "root", {
		"host" : "localhost",
		dialect : "mysql"
	});
	
	function getConnection(){
		return sequelize;
	}

	module.exports["getConnection"] = getConnection;
	
	/*
	module.exports["mediator"] = require("./ticket/Mediator");
	module.exports["credential"] = require("./ticket/Credentials");
	module.exports["product"] = require("./ticket/Product");
	module.exports["client"] = require("./ticket/Client");
	module.exports["entry"] = require("./ticket/Entry");
	module.exports["ticket"] = require("./ticket/Ticket");
	module.exports["ticketProduct"] = require("./ticket/TicketProduct");
	*/
	
	// process.exit();