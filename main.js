

	var express = require("express");
	var app = express();

	// publicando os serviços restfull
	require("./src/controller/restfullController")(app);
	
	app.listen("8080");